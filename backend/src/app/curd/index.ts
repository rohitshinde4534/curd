import * as controllers  from './controller';
import {RoutesBuilder } from '../../config/routes_config';
import {curdSchema} from './schema';
import { validationPaginationParams } from '../middlewares/pagination';

export const Curd:RoutesBuilder = (router) =>{
    	/**
	 * @openapi
	 * /admin_user:
	 *   post:
	 *     tags:
	 *       - Admin User API
	 *     description: Employee login route in admin_user
	 *     requestBody:
	 *       description: admin_user details for creation
	 *       content:
	 *         application/json:
	 *           schema:
	 *             type: object
	 *             properties:
	 *               user_name:
	 *                 type: string
	 *                 example: 'ashish'
	 *     responses:
	 *       422:
	 *         description: Unprocessable request
	 *       400:
	 *         description: Validation errors
	 *       200:
	 *         description: Returns success on update
	 *       201:
	 *         description: Returns success on update
	 */
    router.post('/curd',controllers.addData);
		/**
	 * @openapi
	 * /curd:
	 *   get:
	 *     tags:
	 *       - curd
	 *     description: Retrieves audit records for a specific table.
	 *     parameters:
	 *       - in: query
	 *         name: per_page
	 *         description: Retrieves a specific microsite_master record by id.
	 *         schema:
	 *           type: integer
	 *       - in: query
	 *         name: current_page
	 *         description:  The current page number starting with 0.
	 *         schema:
	 *           type: integer
	 *     responses:
	 *       200:
	 *         description: Successful Response
	 *         content:
	 *           application/json:
	 *             schema:
	 *               type: Object
	 *               properties:
	 *                 table_name:
	 *                   type:enum
	 *                 table_key:
	 *                   type:integer
	 *                 status:
	 *                   type:enum
	 *                 per_page:
	 *                   type:integer
	 *                 current_page:
	 *                   type:integer
	 *             example:
	 *               table_name:['*'],
	 *               table_key:21,
	 *               status:['pending'],
	 *               per_page:10,
	 *               current_page:0
	 *       '404':
	 *         description: audit not found
	 */
    router.get('/curd',validationPaginationParams("customers"),controllers.getData);
}
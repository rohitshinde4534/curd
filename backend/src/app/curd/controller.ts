import { StatusCodes } from "http-status-codes";
import { knex_connection } from "../../knex";
import { logger } from "../../utils/logger";
import { RequestHandler } from "express";
import { ApplicationErrors, makeError } from "../../commons/errors";
import { accessorPaginationInfo } from "../middlewares/pagination";
import {parseJSON} from "../../utils/json";

export const addData: RequestHandler = async (req, res, next) => {
	try {
		const { user_name } = req.body;
		console.log(user_name);
		const data = await knex_connection("admin_user")
			.select("*")
			.where({
				user_name: user_name,
			})
			.first();
		if (data == undefined || data == null || data == "") {
			return res
				.status(StatusCodes.BAD_REQUEST)
				.json(makeError(ApplicationErrors.NOT_FOUND, "admin_user not found"));
		}
		console.log(data);
		return res
			.status(StatusCodes.OK)
			.json({ user_name: user_name, user_role: data.user_role });
	} catch (error) {
		logger("emp_list").error(error);
		return res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json(makeError(ApplicationErrors.UNEXPECTED_ERROR));
	}
};

export const getData: RequestHandler = async(req,res,next) => {
	try{
		const current_page = +(req.query.current_page || "0");
		const per_page = +(req.query.per_page || "10");
        const pagination = accessorPaginationInfo(req).data;
		const table_name = req.params.table_name;
	    const data = await knex_connection("customers").select("*").limit(per_page)
		.offset(current_page);;
	if(!data){
		return res.status(StatusCodes.NOT_FOUND).json(makeError(ApplicationErrors.UNEXPECTED_ERROR,"Data not found"))
	}
   await data.forEach(row =>{
		console.log(parseJSON(JSON.stringify(row.customers)))
	})
	return res.status(StatusCodes.OK).json({data,pagination:pagination})
	}catch(error){
		console.log('3')
		logger("curd").error(error);
		return res.status(StatusCodes.BAD_REQUEST).json(error)
	}
}


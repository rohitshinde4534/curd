import Joi from "joi";

/**
 * @openapi
 * components:
 *   schemas:
 *     AdminUserSchemas:
 *       type: object
 *       required:
 *         - user_role
 *       properties:
 *         user_role:
 *           type: string
 *           description: user_role of admin_user
 *       example:
 *         user_role: 'Ashish'
 */
export const curdSchema = Joi.object({
	user_name: Joi.string().max(250).required(),
});
